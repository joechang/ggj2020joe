﻿using Invector.vCharacterController;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JC_Sneeze : MonoBehaviour
{
    public ParticleSystem Sneeze;
    public AudioSource SneezeAudio;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "enemy")
        {
            if (Sneeze.isEmitting)
            {
                var ai = other.GetComponent<JC_ThirdPersonAI>();

                if (ai.infectionstatus)
                {
                    ai.infectionstatus = false;
                    Debug.Log("You are an onion.");
                }
            }
        }
    }
    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.C))
        {
            Sneeze.Play();
            SneezeAudio.Play();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JW_Cured : MonoBehaviour
{
    public GameObject body;
    public bool isSick = false;
    public Material sickMat;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //sneezed on
        if (!GetComponent<Invector.vCharacterController.JC_ThirdPersonAI>().infectionstatus && !isSick)
        {
            isSick = true;


            body.GetComponent<SkinnedMeshRenderer>().materials = new Material[] { sickMat, sickMat, sickMat, sickMat, sickMat };
            
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Invector.vCharacterController;

public class JC_WibbleWobble : MonoBehaviour
{    
    public Slider Slider;
    public Text infectioncount;
    public Transform TransformToWibbleWobble;
    public KeyCode keyCodeWibble = KeyCode.Q;
    public KeyCode keyCodeWobble = KeyCode.E;
    public GameObject panel;

    
    
    public float Intensity = .005F;    
    public float Wobble = .005f;

    public float EffectSpeed = .05f;

    private bool isWibble;

    private System.DateTime lastChange;    

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
        lastChange = System.DateTime.MinValue;

        if(TransformToWibbleWobble == null)
        {
            Debug.LogError("Unable to find the player game object");
        }

        if (Slider == null)
        {
            Debug.LogError("Unable to find the slider!");
        }

        InvokeRepeating("ChangeIntensity", .5f, .5f);

        InvokeRepeating("WibbleWobble", EffectSpeed, EffectSpeed);
    }

    private void Update()
    {
        if (Input.GetKeyDown(keyCodeWibble)) Slider.value += Wobble;
        if (Input.GetKeyDown(keyCodeWobble)) Slider.value -= Wobble;

        var enemies = GameObject.FindGameObjectsWithTag("enemy").Select(go => go.GetComponent<JC_ThirdPersonAI>()).ToArray();
        var infected = enemies.Where(enemy => enemy.infectionstatus == true).Count();

        infectioncount.text = infected.ToString();

        if (infected == 0)
        {
            panel.SetActive(true);
            Time.timeScale = 0;
        }
    }

    void ChangeIntensity()
    {
        if(System.DateTime.Now.Subtract(lastChange) > System.TimeSpan.FromSeconds(10))
        {
            lastChange = System.DateTime.Now;
            Intensity = Intensity * (Random.value > 0.5f ? 1f : -1f);
        }

        Slider.value += Intensity;
    }

    void WibbleWobble()
    {
        var rotation = TransformToWibbleWobble.rotation;

        if (isWibble)
        {
            TransformToWibbleWobble.rotation = new Quaternion(rotation.x + Slider.value, rotation.y, rotation.z, rotation.w + Slider.value);
        }
        else
        {
            TransformToWibbleWobble.rotation = new Quaternion(rotation.x - Slider.value, rotation.y, rotation.z, rotation.w - Slider.value);
        }

        isWibble = !isWibble;
    }
public void buttonstart()
    {
        panel.SetActive(false);
        Time.timeScale = 1;
        var enemies = GameObject.FindGameObjectsWithTag("enemy").Select(go => go.GetComponent<JC_ThirdPersonAI>()).ToArray();
        foreach(var enemy in enemies)
        {
            enemy.infectionstatus = true;
        }

    }
}

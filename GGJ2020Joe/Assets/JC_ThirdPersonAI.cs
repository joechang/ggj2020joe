﻿using System.Linq;

using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

namespace Invector.vCharacterController
{
    public enum AI_STATE { IDLE, WALKTO, RUNTO }

    public class JC_ThirdPersonAI : MonoBehaviour
    {
        public int NumberOfSecondsToMakeADecision;

        public bool infectionstatus = true;

        private Transform[] TargetTransforms;

        public AI_STATE State = AI_STATE.IDLE;

        private System.DateTime LastDecision;
        private System.DateTime LastAware;

        private int Target;

        AICharacterControl CharacterControl;

        public void Start()
        {
            NumberOfSecondsToMakeADecision = Random.Range(1, 10);
            LastDecision = System.DateTime.Now;

            CharacterControl = this.GetComponent<AICharacterControl>();

            TargetTransforms = GameObject.FindGameObjectsWithTag("TargetTransform").Select(go => go.transform).ToArray();

        }

        public void Update()
        {
            if((State == AI_STATE.RUNTO && System.DateTime.Now.Subtract(LastAware) > System.TimeSpan.FromSeconds(5)) ||
               System.DateTime.Now.Subtract(LastDecision) > System.TimeSpan.FromSeconds(NumberOfSecondsToMakeADecision))
            {
                LastDecision = System.DateTime.Now;

                State = (AI_STATE)Random.Range(0, 2);
                Target = Random.Range(0, TargetTransforms.Length - 1);

                CharacterControl.SetTarget(TargetTransforms[Target]);
            }






        }


    }
}